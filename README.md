# Sample VR Tour

Demo VR app using [React VR](https://facebook.github.io/react-vr/)

This app demonstrate a simple VR tour. 

Markers are displayed on a sample panoramic image that will display an info box when clicked.

## Running
``$ cd samplevrtour ``

``$ npm install ``

 ``$ npm start ``


### Credits
Photo credit

HamburgerJung [Krake](http://www.flickr.com/photos/83248192@N00/32831402653) via [photopin](http://photopin.com) [(license)](https://creativecommons.org/licenses/by-nc-sa/2.0/)

Icons made by

[Dave Gandy](http://www.flaticon.com/authors/dave-gandy) from [Flaticon](http://www.flaticon.com) licensed by [CC 3.0 BY](http://creativecommons.org/licenses/by/3.0/)

[Freepik](http://www.freepik.com) from [Flaticon](http://www.flaticon.com) licensed by [CC 3.0 BY](http://creativecommons.org/licenses/by/3.0/)

[Google](http://www.flaticon.com/authors/google) from [Flaticon](http://www.flaticon.com) licensed by [CC 3.0 BY](http://creativecommons.org/licenses/by/3.0/)
