import React from 'react';
import {
  asset,
  View,
  Model,
  Image,
  VrButton
} from 'react-vr';

import InfoBox from './InfoBox';

const DEFAULT_ANIMATION_BUTTON_SIZE = 0.08;
const ANIMATION_BUTTON_EXPAND = 0.10;
const ANIMATION_BUTTON_EXPAND_SPEED = 0.001;
const DEFAULT_IMAGE = 'info.png';

export default class DirectionButton extends React.Component {
  constructor() {
    super();
    this.state={
      animationWidth: DEFAULT_ANIMATION_BUTTON_SIZE,
      translate: [0, 0, 0],
      rotate: [0, 0, 0],
      showToolTip: false,
    };
    this.animatePointer = this.animatePointer.bind(this);
    this.handleOnEnter = this.handleOnEnter.bind(this);
    this.handleOnExit = this.handleOnExit.bind(this);
  }
  componentDidMount(){
    this.animatePointer();
  }
  componentWillUnmount(){
    if (this.frameHandle) {
     cancelAnimationFrame(this.frameHandle);
     this.frameHandle = null;
    }
  }
  animatePointer() {
    var delta = this.state.animationWidth + ANIMATION_BUTTON_EXPAND_SPEED;
    if(delta >= ANIMATION_BUTTON_EXPAND){
      delta = DEFAULT_ANIMATION_BUTTON_SIZE;
    }
    this.setState({animationWidth: delta});
    this.frameHandle = requestAnimationFrame(this.animatePointer);
  }
  handleOnEnter(item) {
    if (item.toolTip) 
      this.setState({
        showToolTip: true,
      })
  }
  handleOnExit() {
    this.setState({
      showToolTip: false,
    })
  }
  render() {
    const that = this;
    const {translate, rotate, scale, handleDirectionClick, item} = this.props;
    let btnImage = this.props.image ? this.props.image : DEFAULT_IMAGE;
    return (
      <Model
        style={{
          position: 'absolute',
          transform: [
            {translate: translate},
            {rotateZ: rotate[2]},
            {rotateY: rotate[1]},
            {rotateX: rotate[0]},
          ]
        }}
        
      >
      {
        this.state.showToolTip && item.toolTip &&
        <View
          style={{
            position: 'absolute'
          }}
        >
          <InfoBox infoBox={item.toolTip} />
        </View>
      }
        <VrButton
          style={{
            alignItems: 'center',
            justifyContent: 'center',
            position: 'absolute'
          }}
          onClick={handleDirectionClick.bind(this, item)}
          onTap={handleDirectionClick.bind(this, item)}
          onEnter={this.handleOnEnter.bind(this, item)}
          onExit={this.handleOnExit.bind(this, item)}
        >
          <Image 
            style={{
              width:0.05,
              height:0.05,
              position: 'absolute',
            }}
            source={asset(btnImage)}
          >
          </Image>
          <Image 
            style={
              {
                width:that.state.animationWidth, 
                height:that.state.animationWidth,
                position: 'absolute',
              }
            }
            source={asset('circle-shape.png')}
          >
          </Image>
        </VrButton>
      </Model>    
    )
  }
}