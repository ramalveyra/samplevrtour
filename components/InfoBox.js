import React from 'react';
import {
  asset,
  Model,
  Image,
  View,
  Text,
  Animated,
} from 'react-vr';

const BOX_OPACITY = 0.7;
export default class InfoBox extends React.Component {
  constructor(props) {
    super(props);
     this.state = {
       fadeAnim: new Animated.Value(0), // init opacity 0
     };
  }
  componentDidMount() {
     Animated.timing(          // Uses easing functions
       this.state.fadeAnim,    // The value to drive
       {toValue: 1}            // Configuration
     ).start();                // Don't forget start!
  }
  render() {
    const { infoBox } = this.props;
    return (
      <Animated.View
        style={{
          opacity: this.state.fadeAnim,
          alignItems: 'center',
          justifyContent: 'center',
          transform: [
            {translate: infoBox.translate},
            {rotateY: infoBox.rotateY},
            {rotateX: infoBox.rotateX},
            {rotateZ: infoBox.rotateZ},
          ]
        }}
      >
        <View>
          <Image 
            style={{
              width: infoBox.boxWidth,
              height: infoBox.boxHeight,
              position: 'absolute',
              opacity: infoBox.opacity === undefined ? BOX_OPACITY : infoBox.opacity,
            }}
            source={asset('black-check-box.png')}
          />
          <Text
            style={{
              width: infoBox.textWidth,
              fontSize: infoBox.fontSize,
              justifyContent: 'center',
              marginLeft: infoBox.textMarginLeft,
              marginTop: infoBox.textMarginTop,
            }}
          >
            { infoBox.text }
          </Text>
        </View>
      </Animated.View>
    )
  }
}
