// Auto-generated content.
// This file contains the boilerplate to set up your React app.
// If you want to modify your application, start in "index.vr.js"

// Auto-generated content.
import {VRInstance} from 'react-vr-web';
import {get3DPoint, radToDeg, degToRad} from '../helpers/cameraHelper.js';
import {Module} from 'react-vr-web';
import  {ReactNativeContext} from 'react-vr-web';
import * as THREE from 'three';
function init(bundle, parent, options) {
  const buttonModule = new ButtonModule();
  const teleportModule = new TeleportModule();
  const vr = new VRInstance(bundle, 'SampleVRTour', parent, {
    // Add custom options here
    // cursorEnabled: true,
    nativeModules: [buttonModule, teleportModule],
    ...options,
  });
  buttonModule.rnctx = vr.rootView.context;
  teleportModule.setCamera(vr.player.camera);
  vr.render = function() {
    // Any custom behavior you want to perform on each frame goes here
  };
  // Begin the animation loop
  vr.start();
  window.playerCamera = vr.player._camera;
  window.vr = vr;
  window.ondblclick= onRendererDoubleClick;
  return vr;
}

class TeleportModule extends Module {
  constructor() {
    super('TeleportModule');
    this._camera = null;
  }

  setCamera(camera) {
    this._camera = camera;
  }

  teleportCamera(x, y, z) {
    if (this._camera) {
      this._camera.rotation.set(degToRad(x), y, z);
      // Call this to make sure anything positioned relative to the camera is set up properly:
      this._camera.updateMatrixWorld(true);
    }
  }
}

export default class ButtonModule extends Module {
  constructor (rnctx: ReactNativeContext) {
    super('ButtonModule');
  }
  getRotation(cb) {
    let rotationX = window.playerCamera.rotation.x;
    let rotationY = window.playerCamera.rotation.y;
    // Convert
    rotationX = radToDeg(rotationX);
    rotationY = radToDeg(rotationY);
    this.rnctx.invokeCallback(cb, [{
      rotationX,
      rotationY,
    }]);
  }
  updateFov(value) {
    window.playerCamera.fov = value;
    window.playerCamera.updateProjectionMatrix();
  }
}

window.ReactVR = {init};

// Using this to easily location coordinates
function onRendererDoubleClick(){
  var x  = 2 * (event.x / window.innerWidth) - 1;
  var y = 1 - 2 * ( event.y / window.innerHeight );
  var coordinates = get3DPoint(window.playerCamera, x, y);
  let rotationX = window.playerCamera.rotation.x;
  let rotationY = window.playerCamera.rotation.y;
  // Convert
  rotationX = radToDeg(rotationX);
  rotationY = radToDeg(rotationY);
  console.log({coordinates, rotationX, rotationY}, window.playerCamera);
}
