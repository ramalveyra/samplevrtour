import React from 'react';
import {
  AppRegistry,
  asset,
  Scene,
  Pano,
  View,
  NativeModules,
} from 'react-vr';

import DirectionButton from './components/DirectionButtons';
import InfoBox from './components/InfoBox';

const ButtonModule = NativeModules.ButtonModule;
const TeleportModule = NativeModules.TeleportModule;
const ANIMATION_CAM_MOVEMENT_SPEED = 4;

export default class SampleVRTour extends React.Component {
  constructor(props) {
    super();
    this.state = {
      current_scene: {},
      sceneLoaded: false,
      scenes: [
        {
          id: 1,
          image: 'main.jpg',
          rotateY: -5,
          directions: [
            {
              id: 1,
              title: "Kasse",
              button: {
                image: 'up-chevron-b.png',
                translate: [0.06, -0.27, -0.9],
                rotate: [0, 0, 0],
              },
              target: 2,
              toolTip: {
                translate: [0.2, -0.2, -0.9],
                rotateX: 0,
                rotateY: -7,
                rotateZ: 0,
                boxWidth: 0.7,
                boxHeight: 0.1,
                textWidth: 0.7,
                textMarginLeft: 0.005,
                textMarginTop: 0.01,
                fontSize: 0.09,
                text: 'Explore KASSE',
                opacity: 0,
              }
            },
            {
              id: 2,
              title: "Mouse",
              button: {
                image: 'up-chevron-b.png',
                translate: [-0.9, -0.08, 0.5],
                rotate: [0, -50, 0],
              },
              target: 3,
              toolTip: {
                translate: [-0.9, 0.05, 0.5],
                rotateX: 0,
                rotateY: 180,
                rotateZ: 0,
                boxWidth: 0.7,
                boxHeight: 0.1,
                textWidth: 0.7,
                textMarginLeft: 0.005,
                textMarginTop: 0.01,
                fontSize: 0.08,
                text: 'Explore MAUS',
                opacity: 0,
              }
            },
            {
              id: 3,
              title: "Shaker",
              button: {
                image: 'up-chevron-b.png',
                translate: [0.45, -0.08, 0.5],
                rotate: [0, 60, 0],
              },
              target: 4,
              toolTip: {
                translate: [-0.98, 0.01, 0.5],
                rotateX: 0,
                rotateY: 180,
                rotateZ: 0,
                boxWidth: 0.7,
                boxHeight: 0.1,
                textWidth: 0.7,
                textMarginLeft: 0.005,
                textMarginTop: 0.01,
                fontSize: 0.08,
                text: 'Explore SHAKER',
                opacity: 0,
              }
            },
          ],
          defaultFov: 80,
        },
        {
          id: 2,
          image: 'kasse.jpg',
          rotateY: 0,
          defaultFov: 60,
          attractions: [
            {
              id: 1,
              title: "Octofuss",  
              button: {
                translate: [0.2, 0.65, -0.9],
                rotate: [0,0,0],
              },
              newRotationY: -10,
              newRotationX: 0,
              zoomValue: 50,
              infoBox: {
                translate: [0.055, 0.145, -0.2],
                rotateX: 15,
                rotateY: -10,
                rotateZ: 0,
                boxWidth: 0.11,
                boxHeight: 0.1,
                textWidth: 0.1,
                textMarginLeft: 0.003,
                textMarginTop: 0.01,
                fontSize: 0.006,
                text: '"OCTOFUSS" \n\n In at turpis ac enim tempus maximus posuere eget sapien. Aenean pellentesque, sapien sit amet tempor vulputate, orci eros lobortis nulla, vel molestie quam justo at tellus. Quisque non nulla tincidunt, iaculis ipsum eget, convallis dolor. Proin at lobortis diam, eu luctus eros. Vivamus quis enim risus.'
              }
            },
            {
              id: 2,
              title: "Irre",
              button: {
                translate: [0.7,-0.2,-0.7],
                rotate: [0, -40, 0],
              },
              newRotationY: -45,
              newRotationX: 0,
              zoomValue: 40,
              infoBox: {
                translate: [0.43, 0.06, -0.3],
                rotateX: 0,
                rotateY: -50,
                rotateZ: 0,
                boxWidth: 0.15,
                boxHeight: 0.15,
                textWidth: 0.14,
                textMarginLeft: 0.005,
                textMarginTop: 0.01,
                fontSize: 0.010,
                text: '"IRRE" \n\n Curabitur eget mi arcu. Phasellus tellus lacus, iaculis vitae rutrum eu, suscipit non elit. Donec et eros non tellus ullamcorper maximus tincidunt sed arcu. Etiam aliquet elit vitae ipsum sagittis, nec fringilla metus aliquam. Donec et condimentum nunc.'
              }
            },
            {
              id: 4,
              title: "Klasse",
              button: {
                translate: [0.9, -0.15, -0.27],
                rotate: [0, 110,-5],
              },
              newRotationY: -70,
              newRotationX: 0,
              zoomValue: 30,
              infoBox: {
                translate: [0.8, 0.06, -0.1],
                rotateX: 0,
                rotateY: -80,
                rotateZ: 0,
                boxWidth: 0.18,
                boxHeight: 0.18,
                textWidth: 0.16,
                textMarginLeft: 0.007,
                textMarginTop: 0.01,
                fontSize: 0.012,
                text: '"KLASSE" \n\n Maecenas blandit sem in dolor convallis ultrices. Nullam vulputate nisi id est varius ornare. Sed eget neque eu sem tincidunt bibendum. Etiam hendrerit lacus mauris, id cursus eros finibus vel. Fusce quis mattis turpis, sed faucibus nisl.'
              }
            },
            {
              id: 5,
              title: "Stark",
              button: {
                translate: [-0.55, -0.22, -0.8],
                rotate: [0,0,0],
              },
              newRotationY: 37,
              newRotationX: 0,
              zoomValue: 65,
              infoBox: {
                translate: [-0.15, 0.06, -0.4],
                rotateX: 0,
                rotateY: 40,
                rotateZ: 0,
                boxWidth: 0.18,
                boxHeight: 0.18,
                textWidth: 0.16,
                textMarginLeft: 0.007,
                textMarginTop: 0.01,
                fontSize: 0.012,
                text: '"STARK" \n\n Phasellus nisi arcu, scelerisque ac sollicitudin sed, lacinia ut purus. Mauris ac rhoncus enim. Ut egestas augue risus, sed venenatis purus convallis nec. Morbi aliquam eros et porta scelerisque. Cras tincidunt pretium mi, ac consequat metus bibendum in. Fusce a viverra neque.'
              }
            },
            {
              id: 3,
              title: "Big Monster",
              button: {
                translate: [-0.8,-0.2,-0.09],
                rotate: [0, 70 , 0],
              },
              newRotationY: 65,
              newRotationX: -10,
              zoomValue: 40,
              infoBox: {
                translate: [-0.6, 0.06, -0.3],
                rotateX: 0,
                rotateY: 70,
                rotateZ: 0,
                boxWidth: 0.18,
                boxHeight: 0.18,
                textWidth: 0.16,
                textMarginLeft: 0.007,
                textMarginTop: 0.01,
                fontSize: 0.012,
                text: '"BIG MONSTER" \n\n Phasellus nisi arcu, scelerisque ac sollicitudin sed, lacinia ut purus. Mauris ac rhoncus enim. Ut egestas augue risus, sed venenatis purus convallis nec. Morbi aliquam eros et porta scelerisque. Cras tincidunt pretium mi, ac consequat metus bibendum in. Fusce a viverra neque.'
              }
            },
          ],
          backToMain: {
            button: {
              image: 'down-chevron.png',
              translate: [0, -0.2, -0.5],
              rotate: [0, 0, 0],
            },
            target: 1,
          },
        },
        {
          id: 3,  
          image: 'mouse.jpg',
          rotateY: 0,
          defaultFov: 120,
          attractions: [
            {
              id: 1,
              title: 'Wild Maus',
              button: {
                translate: [-0.4, 0.23, -0.3],
                rotate: [0, 0 , 0],
              },
              newRotationY: 0,
              newRotationX: 0,
              zoomValue: 120,
              infoBox: {
                translate: [-0.3, 0.07, -0.15],
                rotateX: 0,
                rotateY: 0,
                rotateZ: 0,
                boxWidth: 0.22,
                boxHeight: 0.17,
                textWidth: 0.19,
                textMarginLeft: 0.03,
                textMarginTop: 0.01,
                fontSize: 0.012,
                text: '"WILD MAUS" \n\n Alterum fierent maiestatis ne cum, ad pri solet voluptatum. Aperiam aliquando dissentiet pro ne, an ius movet deleniti philosophia. Mel harum placerat consetetur no. Docendi urbanitas vim ut. Eros munere eam ex, nam an nibh detraxit incorrupte, eam ei torquatos contentiones.'
              }
            }
          ],
          backToMain: {
            button: {
              image: 'down-chevron.png',
              translate: [0.2, -0.4, -0.3],
              rotate: [0, 0, 0],
            },
            target: 1,
          },
        },
        {
          id: 4,
          image: 'rock.jpg',
          rotateY: 0,
          defaultFov: 80,
          attractions: [
            {
              id: 1,
              title: 'Rock',
              button: {
                translate: [0.3, 0.5, -0.9],
                rotate: [0, 0 , 0],
              },
              newRotationY: -20,
              newRotationX: 0,
              zoomValue: 80,
              infoBox: {
                translate: [0.1, 0.15, -0.2],
                rotateX: -3,
                rotateY: -20,
                rotateZ: 0,
                boxWidth: 0.18,
                boxHeight: 0.18,
                textWidth: 0.16,
                textMarginLeft: 0.007,
                textMarginTop: 0.01,
                fontSize: 0.012,
                text: '"ROCK!" \n\n Sea ea veniam mediocritatem, eruditi mnesarchum duo at. Est nonumes vivendum cu, ne quod quaerendum qui. An pri tale graece epicurei, pri paulo mentitum intellegat id. Ea sea meliore docendi comprehensam, ex duo elit interpretaris.'
              }
            }
          ],
          backToMain: {
            button: {
              image: 'down-chevron.png',
              translate: [0, -0.34, -0.5],
              rotate: [0, 0, 0],
            },
            target: 1,
          },
        }
      ],
      selectedItem: null,
      showInfoBox: false,
      
    }
    this.handleDirectionClick = this.handleDirectionClick.bind(this);
    this.handleNavigationClick = this.handleNavigationClick.bind(this);
    this.animateCameraMovement = this.animateCameraMovement.bind(this);
    this.handleOnLoad = this.handleOnLoad.bind(this);
    this.handleOnLoadEnd = this.handleOnLoadEnd.bind(this);
  }

  componentWillMount() {
    this.setState({
      current_scene: this.state.scenes[0],
      rotateY: this.state.scenes[0].rotateY,
    });
  }

  animateCameraMovement(target, cb) {
    const rIndex = 'rotate'+target;
    const diffIndex = 'diff'+target;
    const rotate = this.state[rIndex];
    let diff = this.state[diffIndex];
    let delta = 0;
    if (rotate !== diff) {
      if (rotate < diff) {
        delta = rotate + ANIMATION_CAM_MOVEMENT_SPEED;
        if ((diff - delta) < ANIMATION_CAM_MOVEMENT_SPEED) {
          delta = diff;
        }
      } else {
        delta= rotate - ANIMATION_CAM_MOVEMENT_SPEED;
        if ((delta - diff) < ANIMATION_CAM_MOVEMENT_SPEED) {
          delta = diff;
        }
      }
      const newState = {};
      newState[rIndex] = delta;
      this.setState(newState); 
      this.frameHandle = requestAnimationFrame(this.animateCameraMovement.bind(this, target, cb));
    } else {
      cb();
    }
  }
  getNewRotation(userRotation, targetRotation) {

  }
  handleNavigationClick(item, e){
    const new_scene = this.state.scenes[item.target - 1];
    this.setState({
      sceneLoaded: false,
      showInfoBox: false,
      current_scene: new_scene,
    });
  }
  handleDirectionClick(item, e) {
      const self = this;
      this.setState({
        showInfoBox: false,
      })
      ButtonModule.getRotation((res)=> {
        const userRotationY = Math.floor(res.rotationY);
        let targetRotationY = Math.floor(item.newRotationY);
        let diffY = targetRotationY - userRotationY;
        const userRotationX = Math.floor(res.rotationX);
        let targetRotationX = Math.floor(item.newRotationX);
        let diffX = targetRotationX - userRotationX;
        this.setState({
          targetRotationY: targetRotationY,
          userRotationY: userRotationY,
          diffY: diffY,
          targetRotationX: targetRotationX,
          userRotationX: userRotationX,
          diffX: diffX,
          selectedItem: item,
        });
        this.animateCameraMovement('Y', ()=>{
          // this.animateCameraMovement('X', ()=>{
            ButtonModule.updateFov(item.zoomValue);
            self.setState({
              showInfoBox: true,
            });
          // });
        });
      });
  }
  handleOnLoad() {
  }
  handleOnLoadEnd() {
    // Reset the rotation
    ButtonModule.getRotation(res=> {
      const userRotationY = Math.floor(res.rotationY);
      ButtonModule.updateFov(this.state.current_scene.defaultFov);
      this.setState({
        sceneLoaded: true,
        rotateY: -userRotationY,
      });  
    })
  }
  render() {
    var that = this;
    const selectedItem = this.state.selectedItem;
    return (
      <View>
        <Pano
          source={asset(this.state.current_scene['image'])}
          onLoad={this.handleOnLoad}
          onLoadEnd={this.handleOnLoadEnd}
        />
        <Scene
         style={{
            transform: [
              {rotateY: this.state.rotateY},
            ],
          }}
        />
        {
          this.state.current_scene.id === 1 && this.state.sceneLoaded ?
            <View>
              {
                this.state.current_scene.directions.map((item, index) => {
                  return <DirectionButton
                    key={index}
                    translate={item.button.translate}
                    rotate={item.button.rotate}
                    handleDirectionClick={this.handleNavigationClick}
                    item={item}
                    image={item.button.image}
                  />
                })
            }
            </View>
          :
          <View>
          {
            !this.state.directionSelected && 
            this.state.sceneLoaded && 
            this.state.current_scene.attractions &&
            this.state.current_scene.attractions.map((item, index)=>{
              return <DirectionButton
                key={index}
                translate={item.button.translate}
                rotate={item.button.rotate}
                handleDirectionClick={this.handleDirectionClick}
                item={item}
              />
            })
          }
          {
            this.state.showInfoBox &&
            <InfoBox infoBox={this.state.selectedItem.infoBox} />
          }
          {
            this.state.sceneLoaded && 
            <DirectionButton
              translate={this.state.current_scene.backToMain.button.translate}
              rotate={this.state.current_scene.backToMain.button.rotate}
              handleDirectionClick={this.handleNavigationClick}
              item={this.state.current_scene.backToMain}
              image={this.state.current_scene.backToMain.button.image}
            />
          }
          </View>
        }
      </View>
    );
  }
};

AppRegistry.registerComponent('SampleVRTour', () => SampleVRTour);
